nodejs
======
This repository contains the edition of the open-source Node.js program used by webOS. Node.js is a server-side JavaScript environment that uses an asynchronous event-driven model. This allows Node.js to get excellent performance based on the architectures of many Internet applications.

See the _Resources for Newcomers_ section in README.upstream.md for links to information on the upstream project. Do not use the build instructions found there.

How to Build on Windows
=====================
## Building

Once you have downloaded the source, execute the following to build it:

    npm install
    npm run start