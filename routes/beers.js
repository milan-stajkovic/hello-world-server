//Dependencies
const express = require('express');
const router = express.Router();
var request = require('request');
var rp = require('request-promise');
var url = require('url');

var baseRequest = {
    uri: 'https://api.punkapi.com/v2/beers',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
};

//Routes

//Beers by ID
router.get("/:a", (req, res) => {
    rp(baseRequest)
        .then(function (repos) {
            const apiID = +req.params.a;
            if (apiID < 0 || !apiID) {
                res.status(400).send("ID has to be a positive whole number!");
            };
            let obj = repos.find(obj => obj.id == apiID);
            res.send(obj);
        })
        .catch(function (err) {
            console.log('error idiot');
        });

});

/*//Query for alcohol and ibu
router.get('/', (req, res) => {
    rp(baseRequest)
        .then(function (repos, qs) {

            var parsedUrl = url.parse(req.url, true);
            var qs = parsedUrl.query;


            //query for alcohol
            if (qs.alc) {

                var lt = qs.alc.includes('lt_');
                var gt = qs.alc.includes('gt_');
                var eq = qs.alc.includes('eq_');

                if (lt) {
                    var alc = parseFloat(qs.alc.slice(3));
                    var obj = repos.filter((element) => {
                        let alcohol = !!(parseFloat(element.abv) <= alc);
                        return alcohol;
                    });
                }

                if (gt) {
                    var alc = parseFloat(qs.alc.slice(3));
                    var obj = repos.filter((element) => {
                        let alcohol = !!(parseFloat(element.abv) >= alc);
                        return alcohol;
                    });
                }

                if (eq) {
                    var alc = parseFloat(qs.alc.slice(3));
                    var obj = repos.filter((element) => {
                        let alcohol = !!(parseFloat(element.abv) == alc);
                        return alcohol;
                    });
                }
            }

            //query for bitterness
            if (qs.ibu) {

                var lt = qs.ibu.includes('lt_');
                var gt = qs.ibu.includes('gt_');
                var eq = qs.ibu.includes('eq_');

                if (lt) {
                    var ibu = parseFloat(qs.ibu.slice(3));
                    var obj = repos.filter((element) => {
                        let bitterness = !!(parseFloat(element.ibu) <= ibu);
                        return bitterness;
                    });
                }

                if (gt) {
                    var ibu = parseFloat(qs.ibu.slice(3));
                    var obj = repos.filter((element) => {
                        let bitterness = !!(parseFloat(element.ibu) >= ibu);
                        return bitterness;
                    });
                }

                if (eq) {
                    var ibu = parseFloat(qs.ibu.slice(3));
                    var obj = repos.filter((element) => {
                        let bitterness = !!(parseFloat(element.ibu) == ibu);
                        return bitterness;
                    });
                }
            }

            //filtered search
            return res.send(obj);
        })
        .catch(function (err) {
            console.log('error idiot!');
        });
});*/

router.get('/', (req, res) => {
    rp(baseRequest)
        .then(function (repos, qs) {
            var parsedUrl = url.parse(req.url, true);
            var qs = parsedUrl.query;
            var qObj = {
                name: qs.name,
                abv: qs.alc,
                ibu: qs.ibu
            };
            if (qs.name || qs.alc || qs.ibu) {
                var result = repos.filter((obj) => {
                    for (var key in qObj) {
                        switch (key) {
                            case value:
                                
                                break;
                        
                            default:
                                if (!!obj[key] && (obj[key] == qObj[key] || typeof obj[key] == 'string' && obj[key].indexOf(qObj[key]) !== -1)) return true;
                                break;
                        }
                    }
                    return false;
                });
            }
            res.send(result);
        })
        .catch(function (err) {
            console.log(err);
        })
});

//Return router
module.exports = router;