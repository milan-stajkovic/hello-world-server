
//Dependencies
const express = require('express');
const router = express.Router();

//Routes
//Triangle API
router.get("/triangle/:a/:b/:c", (req, res) => {
    const s1 = +req.params.a;
    const s2 = +req.params.b;
    const s3 = +req.params.c;
    if (s1<0 || s2<0 || s3<0) res.status(404).send("Negative numbers are not valid values!");
    if (!s1 || !s2 || !s3) res.status(404).send("Please enter only positive numbers");
    const circumference = s1+s2+s3;
    const surface = Math.sqrt(circumference*(circumference-s1)*(circumference-s2)*(circumference-s3))/4;
    const triangle = {
      circumference: "Circumference of given triangle = " + circumference,
      surface: "Surface of given triangle = " + surface,
    };
    res.send(triangle);
  });

//Parallelogram API
router.get("/parallelogram/:a/:b/:h", (req, res) => {
    const s1 = +req.params.a;
    const s2 = +req.params.b;
    const h = +req.params.h;
    if (s1<0 || s2<0 || h<0) res.status(404).send("Negative numbers are not valid values!");
    if (!s1 || !s2 || !h) res.status(404).send("Please enter only positive numbers!");
    const circumference = (2*s1)+(2*s2);
    const surface = s1*h;
    const parallelogram = {
      circumference: "Circumference of given parallelogram = " + circumference,
      surface: "Surface of given parallelogram = " + surface,
    };
    res.send(parallelogram);
  });
  
//Coupe API
router.get("/coupe/:r/:s/:H", (req, res) => {
    const r = +req.params.r;
    const s = +req.params.s;
    const H = +req.params.H;
    if (r<0 || s<0 || H<0) res.status(404).send("Negative numbers are not valid values!");
    if (!r || !s || !H) res.status(404).send("Please enter only positive numbers!");
    const surface = Math.PI*r*(r+s);
    const volume = (Math.pow(r,2)*Math.PI*H)/3;
    const coupe = {
      surface: "Surface of given coupe = " + surface,
      volume: "Volume of given coupe = " + volume,
    };
    res.send(coupe);
  });
  
//Pyramid API
router.get("/pyramid/:a/:h/:H", (req, res) => {
    const s = +req.params.a;
    const h = +req.params.h;
    const H = +req.params.H;
    if (s<0 || h<0 || H<0) res.status(404).send("Negative numbers are not valid values!");
    if (!s || !h || !H) res.status(404).send("Please enter only positive numbers!");
    const B = Math.pow(s,2);
    const M = 2*s*h;
    const surface = B+M;
    const volume = (B*H)/3;
    const pyramid = {
      surface: "Surface of given pyramid = " + surface,
      volume: "Volume of given pyramid = " + volume,
    };
    res.send(pyramid);
  });

//Return router
module.exports = router;