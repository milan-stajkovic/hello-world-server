const express = require('express');
const parser = require('body-parser');
var cors = require('cors');

const PORT = process.env.PORT ? process.env.PORT : 3000;

const app = express();
app.use(cors());
app.use(parser.json());
app.get("/", (req, res) => {
  res.send({
   name:'Hi and Welcome. For each geometric shape add given text (numbers) to url to calculate',
   beers: 'If you want beers API, please add /beers',
    for_triangle: "/api/triangle/side1/side2/side3",
    for_parallelogram: "/api/parallelogram/side1/side2/height1",
    for_coupe: "/api/coupe/radius/slantHeight/height",
    for_pyramid: "/api/pyramid/side/slantHeight/height"
  });
});

//Routes
app.use('/api', require('./routes/api'));
app.use('/beers', require('./routes/beers'));

app.listen(PORT);
